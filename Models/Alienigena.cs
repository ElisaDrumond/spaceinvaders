using System;
using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using Avalonia.Threading;

namespace SpaceInvaders
{
    public class Alienigena
    {
        private AnimatedSprite alienSprite;

        public Alienigena(double positionX, double positionY, List<string> framePaths)
        {
            alienSprite = new AnimatedSprite(framePaths);
            SetInitialPosition(positionX, positionY);

            alienSprite.GetSpriteImage().Width = 160;
        }

        public void StartAnimation()
        {
            alienSprite.StartAnimation();
        }

        public void StopAnimation()
        {
            alienSprite.StopAnimation();
        }


        private void SetInitialPosition(double x, double y)
        {
            alienSprite.GetSpriteImage().Margin = new Avalonia.Thickness(x, y, 0, 0);
        }

        public Image GetAlienSprite()
        {
            return alienSprite.GetSpriteImage();
        }
    }
}
