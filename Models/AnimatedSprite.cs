using System;
using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Media.Imaging;
using Avalonia.Threading;

namespace SpaceInvaders
{
    public class AnimatedSprite
    {
        private Image spriteImage;
        private List<string> framePaths;
        private int currentFrameIndex;
        private DispatcherTimer timer;

        public AnimatedSprite(List<string> framePaths)
        {
            this.framePaths = framePaths;
            spriteImage = new Image();
            currentFrameIndex = 0;

            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += Timer_Tick;
        }

        public void StartAnimation()
        {
            timer.Start();
        }

        public void StopAnimation()
        {
            timer.Stop();
        }

        public Image GetSpriteImage()
        {
            return spriteImage;
        }

        private void Timer_Tick(object? sender, EventArgs e)
        {
            spriteImage.Source = new Bitmap(framePaths[currentFrameIndex]);
            currentFrameIndex = (currentFrameIndex + 1) % framePaths.Count;
        }
    }
}
