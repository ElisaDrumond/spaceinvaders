using Avalonia.Controls;
using Avalonia.Media.Imaging;

public class Escudo
{
    private double positionX;
    private double positionY;
    private Image escudoSprite;

    public Escudo(double initialX, double initialY, string imagePath)
    {
        positionX = initialX;
        positionY = initialY;
    
        escudoSprite = new Image
        {
            Source = new Bitmap(imagePath),
            Width = 200
        };

        SetPosition();
    }

    public void SetPosition()
    {
        Canvas.SetLeft(escudoSprite, positionX);
        Canvas.SetTop(escudoSprite, positionY);
    }

    public Image GetEscudoSprite()
    {
        return escudoSprite;
    }

}