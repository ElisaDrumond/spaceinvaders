using NAudio.Wave;

public class Music
{
    private IWavePlayer player;
    private AudioFileReader audioFile;

    public Music(string audioPath)
    {
        player = new WaveOutEvent();
        audioFile = new AudioFileReader(audioPath);
        player.Init(new LoopStream(audioFile));
        player.Volume = 0.7f;
    }

    public void StartMusic()
    {
        player.Play();
    }

    public void StopMusic()
    {
        player.Stop();
    }

    private class LoopStream : WaveStream
    {
        private readonly WaveStream sourceStream;

        public LoopStream(WaveStream sourceStream)
        {
            this.sourceStream = sourceStream;
            this.EnableLooping = true;
        }

        public bool EnableLooping { get; set; }

        public override WaveFormat WaveFormat => sourceStream.WaveFormat;

        public override long Length => EnableLooping ? long.MaxValue : sourceStream.Length;

        public override long Position
        {
            get => sourceStream.Position;
            set => sourceStream.Position = value;
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int totalBytesRead = 0;

            while (totalBytesRead < count)
            {
                int bytesRead = sourceStream.Read(buffer, offset + totalBytesRead, count - totalBytesRead);

                if (bytesRead == 0)
                {
                    if (EnableLooping)
                    {
                        sourceStream.Position = 0;
                    }
                    else
                    {
                        break;
                    }
                }
                else
                {
                    totalBytesRead += bytesRead;
                }
            }

            return totalBytesRead;
        }
    }
}
