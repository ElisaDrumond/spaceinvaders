using System;
using System.Collections.Generic;
using Avalonia.Controls;

namespace SpaceInvaders

{
    public class Nave
    {
        private AnimatedSprite naveSprite;
        private Canvas container;

        private double positionX;
        private double positionY;
        private double moveSpeed = 8;
        
        private double screenWidth = 1600;
        private double screenHeight = 1080;

        public Nave(Canvas container)
        {
            this.container = container;

            List<string> framePaths = new List<string>
            {
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/nave/Nave-1.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/nave/Nave-2.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/nave/Nave-3.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/nave/Nave-4.png",
            };

            naveSprite = new AnimatedSprite(framePaths);
            naveSprite.GetSpriteImage().Width = 180;

            positionX = 8; 
            positionY = 650;

            SetPosition();
        }

        public void StartAnimation()
        {
            naveSprite.StartAnimation();
        }

        public void StopAnimation()
        {
            naveSprite.StopAnimation();
        }

        public void AddToCanvas()
        {
            container.Children.Add(naveSprite.GetSpriteImage());
        }

        public void SetPosition()
        {
            positionX = Math.Max(0, Math.Min(positionX, screenWidth - naveSprite.GetSpriteImage().Bounds.Width));
            positionY = Math.Max(0, Math.Min(positionY, screenHeight - naveSprite.GetSpriteImage().Bounds.Height));

            Canvas.SetLeft(naveSprite.GetSpriteImage(), positionX);
            Canvas.SetTop(naveSprite.GetSpriteImage(), positionY);
        }

        public void MoveLeft()
        {
            positionX = Math.Max(0, positionX - moveSpeed);
            SetPosition();
        }
        
        public void MoveRight()
        {
            double spriteWidth = naveSprite.GetSpriteImage().Bounds.Width;
            double newPositionX = positionX + moveSpeed;
            double rightLimit = screenWidth - spriteWidth;

            if (newPositionX <= rightLimit)
            {
                positionX = newPositionX;
                SetPosition();
            }
        }
    }
}