using Avalonia.Controls;
using Avalonia.Media.Imaging;


public class Vida
{
    private Image vidaSprite;
    private double positionX;
    private double positionY;

    public Vida(double initialX, double initialY, string imagePath)
    {
        positionX = initialX;
        positionY = initialY;

        vidaSprite = new Image
        {
            Source = new Bitmap(imagePath),
            Width = 30
        };

        SetPosition();
    }

    public void SetPosition()
    {
        Canvas.SetLeft(vidaSprite, positionX);
        Canvas.SetTop(vidaSprite, positionY);
    }

    public Image GetvidaSprite()
    {
        return vidaSprite;
    }
}