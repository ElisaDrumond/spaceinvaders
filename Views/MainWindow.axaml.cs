using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Input;
using System.Collections.Generic;
using System.ComponentModel;


namespace SpaceInvaders.Views
{
    public partial class MainWindow : Window
    {
        private Nave nave;
        private List<Alienigena> alienigenas;

        private Music jogoMusica;
        private List<Vida> vidas;

        public MainWindow()
        {
            InitializeComponent();

            Canvas container = this.FindControl<Canvas>("Container") ?? new Canvas();
            Canvas alienContainer = this.FindControl<Canvas>("AlienContainer") ?? new Canvas();

            jogoMusica = new Music("C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/musics/P1.mp3");
            jogoMusica.StartMusic();

            nave = new Nave(container);
            nave.AddToCanvas();
            nave.StartAnimation();
            nave.SetPosition();

            this.KeyDown += HandleKeyDown;

            AdicionarEscudos(container);

            alienigenas = new List<Alienigena>();
            InitializeAlienigenas(container);

            vidas = new List<Vida>();
            InitializeVidas(container);

        }

        private void AdicionarEscudos(Canvas container)
        {
            AdicionarEscudo(container, 100, 550);
            AdicionarEscudo(container, 440, 550);
            AdicionarEscudo(container, 780, 550);
            AdicionarEscudo(container, 1120, 550);
        }

        private void AdicionarEscudo(Canvas container, double posX, double posY)
        {
            Escudo escudo = new Escudo(posX, posY, "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/escudo/Escudo.png");
            container.Children.Add(escudo.GetEscudoSprite());
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void HandleKeyDown(object? sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left:
                    nave?.MoveLeft();
                    break;
                
                case Key.A:
                    nave?.MoveLeft();
                    break;

                case Key.Right:
                    nave?.MoveRight();
                    break;
                
                case Key.D:
                    nave?.MoveRight();
                    break;
            }
        }
        
        private void InitializeAlienigenas(Canvas alienContainer)
        {
            List<List<string>> tiposAlienigenasFrames = ObterTiposAlienigenasFrames();
            double margemSuperior = 90;
            double espacamentoEntreLinhas = 30;
            
            List<string> alienFramesTipo1 = tiposAlienigenasFrames[0];
            double alienYTipo1 = margemSuperior;

            //Tipo 1
            for (int col = 0; col < 11; col++)
            {
                double alienX = col * 130;
                Alienigena alien = new Alienigena(alienX, alienYTipo1, alienFramesTipo1);
                alien.StartAnimation();
                alienigenas.Add(alien);
                alienContainer.Children.Add(alien.GetAlienSprite());
            }
            
            // Tipo 2
            List<string> alienFramesTipo2 = tiposAlienigenasFrames[1];
            for (int row = 0; row < 2; row++)
            {
                double espacamentoY = row > 0 ? espacamentoEntreLinhas : 0;
                double alienY = 50 + (row * (40 + espacamentoY)) + 110;

                for (int col = 0; col < 11; col++)
                {
                    double alienX = col * 130;
                    Alienigena alien = new Alienigena(alienX, alienY, alienFramesTipo2);
                    alien.StartAnimation();
                    alienigenas.Add(alien);
                    alienContainer.Children.Add(alien.GetAlienSprite());
                }
            }

            //Tipo 3
            List<string> alienFramesTipo3 = tiposAlienigenasFrames[2];
            for (int row = 0; row < 2; row++)
            {
                double espacamentoY = row > 0 ? espacamentoEntreLinhas : 0;
                double alienY = 80 + (row * (40 + espacamentoY)) + 220;


                for (int col = 0; col < 11; col++)
                {
                    double alienX = col * 130;
                    Alienigena alien = new Alienigena(alienX, alienY, alienFramesTipo3);
                    alien.StartAnimation();
                    alienigenas.Add(alien);
                    alienContainer.Children.Add(alien.GetAlienSprite());
                }
            }
        }

        private List<List<string>> ObterTiposAlienigenasFrames()
        {
            List<List<string>> frames = new List<List<string>>();

            frames.Add(new List<string>
            {
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-1.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-2.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-3.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-4.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-5.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-6.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-1/Et1-7.png",
            });

            frames.Add(new List<string>
            {
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-1.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-2.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-3.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-4.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-5.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-6.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-7.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-8.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-9.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-10.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-11.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-12.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-2/Et2-13.png",
            });

            frames.Add(new List<string>
            {
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-1.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-2.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-3.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-4.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-5.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-6.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-7.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-8.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-9.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-10.png",
                "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/et-3/Et3-11.png",
            });

            return frames;
        }

        private void OnClosing(object sender, CancelEventArgs e)
        {
            jogoMusica.StopMusic();
        }

        private void InitializeVidas(Canvas container)
        {
            AdicionarCoracao(container, 100, 30);
            AdicionarCoracao(container, 150, 30);
            AdicionarCoracao(container, 200, 30);

        }

        private void AdicionarCoracao(Canvas container, double posX, double posY)
        {
            Vida vida = new Vida(posX, posY, "C:/Users/Elisa/Projetos/Avalonia/Projeto-Final/SpaceInvaders/SpaceInvaders/Assets/images/coracao/coracao-cheio.png");
            container.Children.Add(vida.GetvidaSprite());
        }
    }
}